package com.core.io.kafka.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class SummaryMsgHandler implements MsgHandler<Object> {

    @Override
    public boolean test(MessageHeaders headers) {
        return true;
    }

    @Override
    public void accept(Object payload, MessageHeaders headers) {
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
