package com.core.io.kafka.handler;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.util.concurrent.TimeUnit;

@Aspect
@Component
public class SummaryMsgHandlerMetrics {

    public static final String TIMER_IN_NAME = "time_in";
    public static final String TIMER_OUT_NAME = "time_out";

    private final MeterRegistry meterRegistry;
    private final Clock clock;
    private final Timer timerIn;
    private final Timer timerOut;

    private static final double[] PERCENTILES = new double[]{.5, .95, .99};

    @Autowired
    public SummaryMsgHandlerMetrics(MeterRegistry meterRegistry, Clock clock) {
        this.meterRegistry = meterRegistry;
        this.clock = clock;
        this.timerIn = Timer.builder(TIMER_IN_NAME)
                .publishPercentiles(PERCENTILES)
                .register(meterRegistry);
        this.timerOut = Timer.builder(TIMER_OUT_NAME)
                .publishPercentiles(PERCENTILES)
                .register(meterRegistry);
    }

    @Around("execution(* com.core.io.kafka.handler.SummaryMsgHandler.accept(..))")
    public void measureTimeAroundAccept(ProceedingJoinPoint joinPoint) throws Throwable {
        long before = clock.millis();

        var summaryTrade = (Object) joinPoint.getArgs()[0];

        joinPoint.proceed();

        timerIn.record(before, TimeUnit.MILLISECONDS);
        timerOut.record(clock.millis(), TimeUnit.MILLISECONDS);
    }
}