package com.core.io.kafka.producer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaProducerException;
import org.springframework.kafka.core.KafkaSendCallback;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class KafkaSender {

    private final KafkaTemplate<String, Object> kafkaTemplate;

    public ListenableFuture<SendResult<String, Object>> send(Message<?> msg) {
        ListenableFuture<SendResult<String, Object>> future = kafkaTemplate.send(msg);
        future.addCallback(getCallback(msg));
        return future;
    }

    private KafkaSendCallback<String, Object> getCallback(Message<?> msg) {
        return new KafkaSendCallback<>() {
            @Override
            public void onSuccess(SendResult<String, Object> objectObjectSendResult) {
                log.info("Successfuly sent");
            }

            @Override
            public void onFailure(KafkaProducerException e) {
                log.error("Failed sending");
                log.error(e.getMessage());
                throw new RuntimeException(e);
            }
        };
    }

}
