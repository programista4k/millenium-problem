package com.core.io.kafka.consumer;

import com.core.io.kafka.handler.MsgHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.KafkaListenerErrorHandler;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.Set;

@KafkaListener(
        topics = {"testtopic"},
        containerFactory = "kafkaListenerContainerFactory",
        errorHandler = "validationErrorHandler")
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CryptoAggListener {

    private final Set<MsgHandler<Object>> handlers;

    @KafkaHandler
    public void listen(@Payload Object payload, @Headers MessageHeaders headers) {
        try {
            handle(payload, headers, handlers);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Bean
    public KafkaListenerErrorHandler validationErrorHandler() {
        return (m, e) -> {
            log.error("validation error");
            return null;
        };
    }

    private <T> void handle(T payload, MessageHeaders headers, Set<MsgHandler<T>> handlers) {
        handlers.stream()
                .filter(h -> h.test(headers))
                .forEach(h -> {
                    h.accept(payload, headers);
                });
    }
}
