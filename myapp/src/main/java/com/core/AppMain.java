package com.core;

import com.core.io.kafka.producer.KafkaSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.MessageBuilder;

import java.util.concurrent.TimeUnit;

@SpringBootApplication
@Slf4j
public class AppMain {

    public static void main(String... args) {
        SpringApplication.run(AppMain.class, args);
    }


    @Autowired
    KafkaSender kafkaSender;

    @Bean
    public CommandLineRunner setUp() {
        return args -> {
            while (true) {
                TimeUnit.MILLISECONDS.sleep(5500);
                kafkaSender.send(MessageBuilder.withPayload("aaa").setHeader(KafkaHeaders.TOPIC, "testtopic").build());
            }
        };
    }

}
