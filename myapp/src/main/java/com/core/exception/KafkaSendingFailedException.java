package com.core.exception;

public class KafkaSendingFailedException extends RuntimeException {

    public KafkaSendingFailedException(Throwable cause) {
        super(cause);
    }
}
