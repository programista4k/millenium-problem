# Problem milenijny

### Na czym polega?  
  
W skrócie
  
2 timery do których zapodaję różny timestamp metrykują taki sam timestamp.  
  
Szczegóły
  
Klasa `com.core.io.kafka.handler.SummaryMsgHandlerMetrics` ma 2 timery `timer_in` i `timer_out`. 
W swojej metodzie `measureTimeAroundAccept(..)` za pomocą aspektu `@Around` oplata metodę `accept(..)` z klasy  `com.core.io.kafka.handler.SummaryMsgHandler`. 
Przed uruchomieniem `accept(..)`, metoda `measureTimeAroundAccept(..)` ma raportować aktualny timestamp do `timer_in`, a po wykonaniu aktualny timestamp do `timer_out`.  
Metoda `accept(..)` jedyne co robi to czeka 500 milisekund, więc `timer_out` powinien być opóźniony w stosunku do `timer_in`.  
Tak się jednak nie dzieje i oba timery mają identyczne wartości co do 1 ms.  

### Jak odwtorzyć problem milenijny?  

0. Miej javę 14 i uruchom dockera.  
1. Uruchom obraz dockerowy `docker-compose-kafka.yml` (to jest cluster kafki) lub kafkę na porcie 19092   
2. Uruchom aplikację javową `myapp`. Co 5500 ms z maina wyśle się wiadomość na Kafkę co uruchomi problematyczną metodę `com.core.io.kafka.handler.SummaryMsgHandler.accept(..)` i metryki z `com.core.io.kafka.handler.SummaryMsgHandlerMetrics.measureTimeAroundAccept(..)`  
3. Poczekaj aż zaloguje się wysyłka `Succesfully sent` czy jakoś tak.  
4. Pod http://localhost:8080 szukaj metryk o nazwie `time_in` i `time_out`  
5. Rozkmiń czemu `time_in` == `time_out` podczas, gdy oczekiwane jest, że `time_in` < `time_out`    
  

### Skrypt powyższych kroków   
  
nie sprawdziaełm czy działa:  
```
docker-compose -f docker-compose-kafka.yml up  

mvn clean install -f myapp  

java -jar .\myapp\target\myapp-1.0.5.jar --spring.config.name=application
```
  
